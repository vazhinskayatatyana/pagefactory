package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class HomePage extends BasePage{

    @FindBy(xpath = "//a[contains (text(),'Уцененные товары')]")
    private WebElement discountGoods;

    @FindBy(xpath = "//span[contains (text(),'Смартфоны и телефоны')]")
    private WebElement smartphonesAndPhonesButton;

    @FindBy(xpath = "//div[@class='brand-box__title']/a[@href='https://avic.ua/smartfonyi' and contains (text(), 'Смартфоны')]")
    private WebElement smartphoneButton;

    public HomePage(WebDriver driver) {
        super(driver);
    }

    public void clickDiscountGoods () {
        discountGoods.click();
    }

    public void clickSmartphonesAndPhonesButton () {
        smartphonesAndPhonesButton.click();
    }

    public void clickSmartphoneButton() {
        smartphoneButton.click();
    }

}
