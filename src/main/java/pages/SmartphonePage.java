package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class SmartphonePage extends BasePage{

    @FindBy(xpath = "//div[@class='prod-cart__descr']")
    private List<WebElement> searchResultsProductsListText;

    @FindBy(xpath = "//a[@class='prod-cart__buy'][contains(@data-ecomm-cart,'Google Pixel 6 8')]")
    private WebElement addToCartButton;

    @FindBy(id = "js_cart")
    private WebElement addToCartPopup;

    @FindBy(xpath = "//span[contains (text(),'Pixel 6 8/256GB')]")
    private WebElement nameOfButton;


    public SmartphonePage(WebDriver driver) {
        super(driver);
    }

    public int getSearchResultsCount() {
        return getSearchResultsList().size();
    }

    public List<WebElement> getSearchResultsList() {
        return searchResultsProductsListText;
    }

    public void clickOnAddToCartButton() {
        addToCartButton.click();
    }

    public WebElement getAddToCartPopup() {
        return addToCartPopup;
    }
    public String getNameOfButton () {
       return nameOfButton.getText();
    }

    }


