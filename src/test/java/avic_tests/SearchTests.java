package avic_tests;

import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class SearchTests extends BaseTest {

    private static final String EXPECTED_SEARCH_QUERY = "discount";

    @Test (priority=1)
    public void checkThatUrlContainsSelected () {
        getHomePage().clickDiscountGoods();
        assertTrue(getDriver().getCurrentUrl().contains(EXPECTED_SEARCH_QUERY));
    }

    @Test (priority = 2)
    public void checkElementsAmountOnSearchPage () {
    getHomePage().clickSmartphonesAndPhonesButton();
    getHomePage().clickSmartphoneButton();
    getHomePage().implicitWait(30);
    assertEquals(getSmartphonePage().getSearchResultsCount(), 12);
    }
}
