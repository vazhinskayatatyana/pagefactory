package avic_tests;

import org.testng.annotations.Test;

import java.time.Duration;

import static org.testng.Assert.assertEquals;

public class AddToCartTest extends BaseTest {

    private static final String  EXPECTED_PRODUCT_IN_CART = "Смартфон Google Pixel 6 8/256GB Stormy Black";

    @Test
    public void checkAddToCart () {
        getHomePage().clickSmartphonesAndPhonesButton();
        getHomePage().clickSmartphoneButton();
        getSmartphonePage().waitForPageLoadComplete(Duration.ofSeconds(30));
        getSmartphonePage().clickOnAddToCartButton();
        getSmartphonePage().waitVisibilityOfElement(Duration.ofSeconds(30),getSmartphonePage().getAddToCartPopup());
        assertEquals (getSmartphonePage().getNameOfButton(), EXPECTED_PRODUCT_IN_CART);

    }

}
